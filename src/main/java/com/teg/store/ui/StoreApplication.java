package com.teg.store.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoreApplication {

	//Class to start the UI
    public static void main(String[] args) {
        SpringApplication.run(StoreApplication.class, args);
    }

}

var wineApp = angular.module('wineApp', [ 'ngRoute', 'ui.carousel', 'ngLodash',
		'ngSanitize', 'ui.router', 'ngAnimate', 'ui.bootstrap', 'ngCookies',
		'ngDialog' ]);

wineApp.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

	$urlRouterProvider.otherwise('/home');

	$stateProvider.state('home', {
		url : "/home",
		views : {
			'@' : {
				templateUrl : 'app/dashboard/main.html'
			},
			'header@home' : {
				templateUrl : 'app/header/header.html',
				controller : 'headerCntrl'
			},
			'main@home' : {
				templateUrl : 'app/main/home.html',
				controller : "homeCntrl"
			},
			'footer@home' : {
				templateUrl : 'app/footer/footer.html',
			// controller: 'footerCntrl'
			}
		}
	}).state('home.storeloc', {
		url : "/storelocator",
		views : {
			'main@home' : {
				templateUrl : 'app/storelocator/storelocator.html',
				controller:"storelocCntrl"
			}
		}
	});
}).run(function($http, $cookies, $rootScope, $location) {

});

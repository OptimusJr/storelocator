wineApp.factory('AppConstants', function() {
    return {
    	
    	COMMON_IP:"http://54.172.95.167:",
    	SERVICE_PORT : "8051",
    	STATE_SERVICE_URL :"/store/bystate/",
    	ZIP_SERVICE_URL : "/store/byzipcode/",
    	CITY_SERVICE_URL : "/store/bycity/",
    	DEFAULT_DIST_SEARCH : "25",
    	SLASH : "/",
    	FALSE : false,
    	TRUE : true,
    	ZIP_REGEX : /^(\d{5})?$/,
    	STATE_REGEX : /^(?:(A[KLRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|P[AR]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY]))$/
    }
});
wineApp.factory('AppService',  ['$http',function ( $http) {
        var service = {};
        service.httpGet = function (url) {
            var returnGet = $http({
                method: "GET",
                url: url,
                cache: true
            });
            return returnGet;
        };
        service.HTTPPost=function(data,url,successCallBack,errorCallBack){
            console.log(data);
            $http({
                method : "POST",
                url : url,
                data:data
            }).success(function(data, status, headers, config){
                if(status === 200 ){
				
                    successCallBack(data);
                } else{
                    console.log("in error back",data);
                    errorCallBack(data);
                }

            }).error(function(data, status, headers, config){
                console.log("in error",data);
                errorCallBack(data);
            });
        };

    return service;

    }]);

	

wineApp.controller('storelocCntrl', ['$scope', '$state', '$cookies', '$location','AppConstants','AppService',
    function($scope, $state, $cookies, $location,AppConstants,AppService) {
		$scope.APPCONSTANTS =AppConstants;
		var commonIP = AppConstants.COMMON_IP;
		var port = AppConstants.SERVICE_PORT;
		var zipCodeRegex = AppConstants.ZIP_REGEX;
		var stateRegex = AppConstants.STATE_REGEX;
		$scope.successResponse = AppConstants.FALSE;
		$scope.serviceRequest = AppConstants.FALSE;
		
		$scope.isSuccess = function(){
			return $scope.successResponse;
		}
		$scope.isService = function(){
			return $scope.serviceRequest;
		}
		
        $scope.search = function(term) {
        	var locatorUrl = commonIP+port;
        	if(zipCodeRegex.test(term)){
        		locatorUrl = locatorUrl+AppConstants.ZIP_SERVICE_URL+term+AppConstants.SLASH+$scope.miles;
        	}
        	else if(stateRegex.test(term)){
        		locatorUrl = locatorUrl+AppConstants.STATE_SERVICE_URL+term;
        	}
        	else{
        		locatorUrl = locatorUrl+AppConstants.CITY_SERVICE_URL+term+AppConstants.SLASH+$scope.miles;
        	}
        	 console.log("locatorUrl: "+locatorUrl);
        	 
        	 AppService.httpGet(locatorUrl).then(function(response){
             	$scope.responseJSON = response.data;
             	$scope.successResponse = AppConstants.TRUE;
             	$scope.serviceRequest = AppConstants.TRUE;
            	var labelIndex = 0;
     		    var locationArray = new Array();
     		    var marker, i;
     		    expr = /byzipcode/;
          		for (i = 0; i < response.data.length; i++) {
              			locationArray[i] = response.data[i].location.coordinates;
     			}            
      			var zoomlevel=7;
          		if(locatorUrl.match(expr))
          			zoomlevel=9;

          		var map = new google.maps.Map(document.getElementById('map'), {
          		      zoom:  zoomlevel,
          		      center: new google.maps.LatLng(locationArray[1][1], locationArray[1][0]),
          		      mapTypeId: google.maps.MapTypeId.ROADMAP
          		    });
          		
          		var sampleText="";
           	    for (i = 0; i < locationArray.length; i++) {  
           	      marker = new google.maps.Marker({
         	        position: new google.maps.LatLng(locationArray[i][1], locationArray[i][0]),
         			label: (i+1).toString(),
         	        map: map
         	      });
           	      if(i<1)
           	    	  sampleText= sampleText+"<button type=\"submit\" id="+i+"\""+" onclick=\"zoomAStore()\" >Go to Location "+ i +"</button>";
              	   document.getElementById("demo").innerHTML=sampleText;

             	}

             })
              .catch(function(response) {
            	 $scope.serviceRequest = AppConstants.TRUE;
            	 $scope.responseJSON = {"length":"0","message":"Please Try a different Store"};
             });
        }
    }]);


//Zoom functionality end

//Directions functionality start
function directionService(){
var directionsDisplay = new google.maps.DirectionsRenderer;
var directionsService = new google.maps.DirectionsService;
calculateAndDisplayRoute(directionsService, directionsDisplay) ;
}
function calculateAndDisplayRoute(directionsService, directionsDisplay) {
  var start = document.getElementById('start').value;
  var end = document.getElementById('end').value;
  directionsService.route({
    origin: start,
    destination: end,
    travelMode: 'DRIVING'
  }, function(response, status) {
    if (status === 'OK') {
      directionsDisplay.setDirections(response);
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}
// End

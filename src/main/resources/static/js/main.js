jQuery(document).ready(
		function($) {
			"use strict";
			$('a[data-rel]').each(function() {
				$(this).attr('rel', $(this).data('rel'));
			});
			if ($('#cp-banner-style-1').length) {
				$('#cp-banner-style-1').bxSlider({
					infiniteLoop : true,
					mode : 'fade',
					auto : true,
					hideControlOnEnd : true
				});
			}
			if ($('#cp-woo-banner').length) {
				$('#cp-woo-banner').bxSlider({
					infiniteLoop : true,
					mode : 'fade',
					auto : true,
					controls : false,
					hideControlOnEnd : true
				});
			}
			if ($('#cp-woo-clients').length) {
				$('#cp-woo-clients').bxSlider({
					infiniteLoop : true,
					mode : 'fade',
					auto : true,
					controls : false,
					hideControlOnEnd : true,
					adaptiveHeight : true,
				});
			}
			if ($('#cp-woo-pro1').length) {
				$('#cp-woo-pro1').bxSlider({
					infiniteLoop : true,
					mode : 'fade',
					auto : true,
					controls : false,
				});
			}
			if ($('#cp-woo-pro2').length) {
				$('#cp-woo-pro2').bxSlider({
					infiniteLoop : true,
					mode : 'fade',
					auto : true,
					controls : true,
					pager : false,
				});
			}
			function attWorkGrid_1() {
				var options = {
					itemWidth : 555,
					autoResize : true,
					container : $('#blog-masonrywrap'),
					offset : 30,
					flexibleWidth : 555
				};
				var handler = $('#blog-masonrywrap li');
				handler.wookmark(options);
			}
			$(window).load(function() {
				attWorkGrid_1();
			});
			$('#blog-masonrywrap li div div a img').load(function() {
				attWorkGrid_1();
			});
			function attWorkGrid_2() {
				var options = {
					itemWidth : 360,
					autoResize : true,
					container : $('#gallery-grid-1-masonrywrap'),
					offset : 30,
					flexibleWidth : 360
				};
				var handler = $('#gallery-grid-1-masonrywrap li');
				handler.wookmark(options);
			}
			$(window).load(function() {
				attWorkGrid_2();
			});
			$('#gallery-grid-1-masonrywrap li div div img').load(function() {
				attWorkGrid_2();
			});
			if ($('#footer-twittes').length) {
				$('#footer-twittes').bxSlider({
					infiniteLoop : true,
					mode : 'fade',
					auto : true,
					hideControlOnEnd : true
				});
			}
			if ($('#content-1').length) {
				$("#content-1").mCustomScrollbar({
					horizontalScroll : true
				});
				$(".content.inner").mCustomScrollbar({
					scrollButtons : {
						enable : true
					}
				});
			}
			if ($('.defaultCountdown').length) {
				var austDay = new Date();
				austDay = new Date(austDay.getFullYear() + 1, 1 - 1, 26);
				$('.defaultCountdown').countdown({
					until : austDay
				});
				$('#year').text(austDay.getFullYear());
			}
			if ($('.gallery').length) {
				$(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({
					animation_speed : 'normal',
					theme : 'light_square',
					slideshow : 3000,
					autoplay_slideshow : true
				});
				$(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({
					animation_speed : 'fast',
					slideshow : 10000,
					hideflash : true
				});
			}
			if ($('audio').length) {
				$('audio').audioPlayer();
			}
			if ($('.gallery-post-1').length) {
				$('.gallery-post-1').bxSlider({
					slideWidth : 600,
					minSlides : 2,
					maxSlides : 3,
					slideMargin : 0
				});
			}
			if ($('#product-detail-slider').length) {
				$('#product-detail-slider').bxSlider({
					pagerCustom : '#bx-pager'
				});
			}
			if ($('.counter').length) {
				$('.counter').counterUp({
					delay : 10,
					time : 1000
				});
			}
			if ($('.brands-slider').length) {
				$('.brands-slider').bxSlider({
					slideWidth : 270,
					minSlides : 2,
					maxSlides : 4,
					slideMargin : 0
				});
			}
			if ($('#calendar').length) {
				var date = new Date();
				var d = date.getDate();
				var m = date.getMonth();
				var y = date.getFullYear();
				$('#calendar').fullCalendar({
					editable : true,
				});
			}
			if ($('#map_contact_2').length) {
				var map;
				var myLatLng = new google.maps.LatLng(48.85661, 2.35222);
				var myOptions = {
					zoom : 12,
					center : myLatLng,
					zoomControl : true,
					mapTypeId : google.maps.MapTypeId.ROADMAP,
					mapTypeControl : false,
					styles : [ {
						saturation : -100,
						lightness : 10
					} ],
				}
				map = new google.maps.Map(document
						.getElementById('map_contact_2'), myOptions);
				var marker = new google.maps.Marker({
					position : map.getCenter(),
					map : map,
					icon : 'images/map-icon.png'
				});
				marker.getPosition();
				var infowindow = new google.maps.InfoWindow({
					content : '',
					position : myLatLng
				});
				infowindow.open(map);
			}
		});